import React from 'react';

import SumSlider from 'components/SumSlider';
import Plans from 'components/Plans';
import TotalBar from 'components/TotalBar';
import DetailedTable from 'components/DetailedTable';

import { Wrapper, Paper } from './styled';

const Calculator = () => (
  <Wrapper>
    <Paper>
      <SumSlider/>
      <Plans/>
      <TotalBar/>
      <DetailedTable/>
    </Paper>
  </Wrapper>
);

export default Calculator;
