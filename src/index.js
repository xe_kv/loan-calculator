import React from 'react';
import ReactDOM from 'react-dom';
import Calculator from './views/Calculator/index';
import { Provider } from 'mobx-react';

import store from './store';

const App = () => (
  <Provider {...store}>
    <Calculator />
  </Provider>
);

ReactDOM.render(<App />, document.getElementById('root'));
