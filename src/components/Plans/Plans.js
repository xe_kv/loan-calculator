import React from 'react';

import PropTypes from 'prop-types';

import SelectField from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';

import { Wrapper } from './styled';

const Plans = ({
  terms,
  term,
  handleSelectPlan,
}) => (
  <Wrapper>
    <InputLabel htmlFor="plan">
    Choose plan
    </InputLabel>
    <SelectField
      floatingLabelText="Plan"
      value={term}
      onChange={handleSelectPlan}
    >
      {
        terms.map(p =>
          <MenuItem key={p} value={p}>
            {`${p} Month`}
          </MenuItem>)
      }
    </SelectField>
  </Wrapper>
);

Plans.propTypes = {
  terms: PropTypes.array,
  term: PropTypes.number,
  monthlyInstallment: PropTypes.number,
  handleSelectPlan: PropTypes.func,
};

export default Plans;
