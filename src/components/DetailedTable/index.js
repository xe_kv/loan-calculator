import {
  compose,
  branch,
  renderNothing,
} from 'recompose';

import { inject, observer } from 'mobx-react';
import DetailedTable from './DetailedTable';

const enhcance = compose(
  inject(stores => ({
    listOfPayments: stores.CalculatorStore.listOfPayments,
    active: stores.CalculatorStore.active,
  })),
  observer,
  branch(
    ({ active }) => !active,
    renderNothing
  ),
);

export default enhcance(DetailedTable);
