const STEPS = [
  { from: 0, to: 500, step: 100 },
  { from: 500, to: 2000, step: 500 },
  { from: 2000, to: 5000, step: 1000 },
];

const PLANS = [
  { term: 3, freeMultiplier: 0.05, maxFee: 91.5, interest: 6 },
  { term: 6, freeMultiplier: 0.03, maxFee: 43.5, interest: 6 },
  { term: 12, freeMultiplier: 0.022, maxFee: 19.5, interest: 6 },
  { term: 24, freeMultiplier: 0.015, maxFee: 7.47, interest: 6 },
  { term: 60, freeMultiplier: 0.01, maxFee: 37.0, interest: 6, from: 2000 },
];

export {
  STEPS,
  PLANS,
};
