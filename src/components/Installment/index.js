import { compose, branch, renderNothing } from 'recompose';
import { inject, observer } from 'mobx-react';

import Installment from './Installment';

const enhance = compose(
  inject(stores => ({
    active: stores.CalculatorStore.active,
    monthlyInstallment: stores.CalculatorStore.monthlyInstallment,
  })),
  observer,
  branch(
    ({ active }) => !active,
    renderNothing
  )
);

export default enhance(Installment);
