import { compose, branch, renderNothing } from 'recompose';
import { inject, observer } from 'mobx-react';

import TotalBar from './TotalBar';

const enhance = compose(
  inject(stores => console.log(stores) || ({
    active: stores.CalculatorStore.active,
    total: stores.CalculatorStore.total,
  })),
  observer,
  branch(
    ({ active }) => !active,
    renderNothing
  )
);

export default enhance(TotalBar);
