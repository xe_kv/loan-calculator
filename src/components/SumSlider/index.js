import { compose, withHandlers } from 'recompose';
import { inject, observer } from 'mobx-react';
import { transaction } from 'mobx';

import assetStep from 'utils/assertStep.js';
import { STEPS } from 'config';

import SumSlider from './SumSlider';

const enhance = compose(
  inject(store => ({
    step: store.CalculatorStore.step,
    amount: store.CalculatorStore.amount,
    setStep: store.CalculatorStore.setStep,
    setAmount: store.CalculatorStore.setAmount,
  })),
  observer,
  withHandlers({
    handleSetValue: ({ setAmount, setStep, amount, step }) => (e, val) => {
      const { step: _step, amount: _amount } = assetStep(amount, val, STEPS);
      if (_amount == amount && step !== _step) return;
      if (_step !== step) {
        transaction(() => {
          setStep(_step);
          setAmount(_amount);
        });
      } else {
        setAmount(val);
      }
    },
  })
);

export default enhance(SumSlider);
