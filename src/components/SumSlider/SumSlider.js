import React from 'react';
import PropTypes from 'prop-types';

import Slider from '@material-ui/lab/Slider';
import { Wrapper } from './styled';

const STATIC = {
  min: 100,
  max: 5000,
};

const SumSlider = ({
  amount,
  step,
  handleSetValue,
}) => (
  <Wrapper>
    {`AMOUNT: ${amount}`}
    <Slider
      value={amount}
      {...STATIC}
      step={step}
      onChange={handleSetValue}
    />
  </Wrapper>
);

SumSlider.propTypes = {
  amount: PropTypes.number,
  step: PropTypes.number,
  handleSetValue: PropTypes.func,
};

export default SumSlider;
