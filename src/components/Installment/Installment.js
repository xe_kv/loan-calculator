import React from 'react';
import PropTypes from 'prop-types';

import { Wrapper } from './styled';

const Installment = ({ monthlyInstallment }) => (
  <Wrapper>
    {`MONTHLY INSTALLMENT: ${monthlyInstallment}`}
  </Wrapper>
);

Installment.propTypes = {
  monthlyInstallment: PropTypes.number,
};

export default Installment;
