import styled from 'styled-components';

import CommonPaper from '@material-ui/core/Paper';

const Wrapper = styled.div`
  margin: 0 auto;
  width: 700px;
`;

const Paper = styled(CommonPaper)`
  padding: 15px;
`;

export {
  Wrapper,
  Paper,
};
