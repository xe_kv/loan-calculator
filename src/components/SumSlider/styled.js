import styled from 'styled-components';

const Wrapper = styled.div`
  width: 97%;
  padding: 0;
  font-size: 1rem;
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  line-height: 1;
`;

export {
  Wrapper,
};
