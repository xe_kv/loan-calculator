import React from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const DetailedTable = ({ listOfPayments }) => (
  <Table>
    <TableHead>
      <TableRow>
        <TableCell>Month</TableCell>
        <TableCell numeric>Capital</TableCell>
        <TableCell numeric>Interest</TableCell>
        <TableCell numeric>Installment</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      {listOfPayments
        .map(({
          capital,
          interest,
          installment,
        }, id) => {
          return (
            <TableRow key={id}>
              <TableCell component="th" scope="row">
                {id + 1}
              </TableCell>
              <TableCell numeric>{capital}</TableCell>
              <TableCell numeric>{interest}</TableCell>
              <TableCell numeric>{installment}</TableCell>
            </TableRow>
          );
        })}
    </TableBody>
  </Table>
);

DetailedTable.propTypes = {
  listOfPayments: PropTypes.array,
};

export default DetailedTable;
