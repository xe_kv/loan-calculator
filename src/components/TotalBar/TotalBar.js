import React from 'react';
import PropTypes from 'prop-types';

import { Wrapper, Text } from './styled';

import Installment from 'components/Installment';

const TotalBar = ({ total }) => (
  <Wrapper>
    <Text>
      {`TOTAL AMOUNT: ${total}`}
    </Text>
    <Installment/>
  </Wrapper>
);

TotalBar.propTypes = {
  total: PropTypes.number,
};

export default TotalBar;
