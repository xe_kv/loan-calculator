function assertStep(prevVal, val, arr) {
  const sets = arr.filter(item => val >= item.from && val <= item.to);
  const { step, from, to } = val >= prevVal ? sets.pop() : sets.shift();
  const amount = [from, to].sort((a, b) => Math.abs(val - a) - Math.abs(val - b))[0];
  return { amount, step };
}

export default assertStep;
