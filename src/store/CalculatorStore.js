import { observable, action, computed } from 'mobx';
import { PLANS } from 'config';

class CalculatorStore {
  @observable amount = 100;
  @observable step = 100;
  @observable term = 3;

  @action.bound setAmount (value) {
    this.amount = value;
  }

  @action.bound setStep (value) {
    this.step = value;
  }

  @action.bound setTerm(value) {
    this.term = value;
  }

  @computed get plan() {
    const _plan = PLANS.filter(p => this.term == p.term);
    if (_plan.length > 0) return _plan.pop();
  }

  @computed get active() {
    if (this.monthlyInstallment <= 5) return false;
    const _plan = PLANS.filter(p => p.from && p.term === this.term).pop();
    if (_plan) {
      if (_plan.from > this.amount || this.monthlyInstallment <= 5) return false;
    }
    return true;
  }

  @computed get monthlyFee() {
    const { freeMultiplier, maxFee } = this.plan;
    if (this.amount * freeMultiplier < maxFee) {
      return this.amount * freeMultiplier;
    }
    return maxFee;
  }

  @computed get total() {
    return Math.round(this.monthlyInstallment * this.term * 100) / 100;
  }

  @computed get monthlyInstallment() {
    const { interest } = this.plan;
    const realRate = Math.trunc(interest * 360 / 365 * 100) / 100;
    const R = realRate / 12 / 100;
    const installment = (this.amount * R) / (1 - (1 + R) ** (this.term * -1));
    return Math.round((installment + this.monthlyFee) * 100) / 100;
  }

  @computed get listOfPayments() {
    const { amount, plan, monthlyInstallment } = this;
    const _list = [];
    const first = {
      capital: amount,
      interest: +(amount * (plan.interest / 100 / 365) * 30).toFixed(6),
      installment: monthlyInstallment,
    };
    _list.push(first);
    for (var i = 1; i < this.term; i++) {
      const _temp = _list[i - 1];
      const capital = +(_temp.capital - (_temp.installment - _temp.interest - this.monthlyFee)).toFixed(2);
      _list.push({
        capital,
        interest: +(capital * (plan.interest / 100 / 365) * 30).toFixed(6),
        installment: monthlyInstallment,
      });
    }
    return _list;
  }
}

export default new CalculatorStore();
