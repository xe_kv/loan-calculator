# LOAN CALCULATOR PROJECT

## Built with
* React
* Styled-components
* Material UI
* MobX
* Recompose

## How to launch?

```
npm i
npm start
```