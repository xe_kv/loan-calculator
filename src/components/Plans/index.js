import {
  compose,
  withHandlers,
  withProps,
} from 'recompose';

import { inject, observer } from 'mobx-react';
import { PLANS } from 'config';

import Plans from './Plans';

const enhance = compose(
  inject(stores => ({
    term: stores.CalculatorStore.term,
    setTerm: stores.CalculatorStore.setTerm,
  })),
  withHandlers({
    handleSelectPlan: ({ setTerm }) => e => setTerm(e.target.value),
  }),
  observer,
  withProps(() => ({
    terms: PLANS.map(p => p.term),
  })),
  withProps(({ term }) => ({
    term: term,
  }))
);

export default enhance(Plans);
